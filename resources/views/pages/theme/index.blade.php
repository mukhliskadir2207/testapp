<x-app-layout>
    <div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">

        <header class="flex justify-between items-center px-5 py-2 border-b border-slate-100 dark:border-slate-700">
            <h2 class="font-semibold text-slate-800 dark:text-slate-100">Themes</h2>
            <button type="button"
                class="px-3 py-2 w-24 text-xs bg-blue-600 text-white hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                data-modal-toggle="addTheme" data-modal-target="addTheme">
                Add
            </button>

        </header>

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            #
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Name
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Status
                        </th>

                        <th scope="col" class="px-6 py-3">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($themes as $index => $theme)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <th scope="row"
                                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $index + 1 }} </th>
                            <td class="px-6 py-4">
                                {{ $theme->name }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $theme->status ?? '' }}
                            </td>

                            <td class="px-6 py-4">
                                <button data-route="{{ route('theme.update', ['id' => $theme->id]) }}"
                                    data-theme-name=" {{ $theme->name }}"
                                    class="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                                    data-modal-toggle="editTheme" data-modal-target="editTheme">Edit</button>

                            </td>

                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        @include('pages.theme.create')
        @include('pages.theme.edit')


    </div>

</x-app-layout>
<script>
    $(document).ready(function() {

        $(document).on('click', '[data-modal-target="editTheme"]', function() {
            var action = $(this).data('route');
            var theme = $(this).data('theme-name');

            $('#editThemeForm').attr('action', action);
            $('#names').val(theme);
        });




    });
</script>
