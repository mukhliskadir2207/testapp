<x-app-layout>
    <div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">

        <header class="flex justify-between items-center px-5 py-2 border-b border-slate-100 dark:border-slate-700">
            <h2 class="font-semibold text-slate-800 dark:text-slate-100">News List</h2>
            <button type="button"
                class="px-3 py-2 w-24 text-xs bg-blue-600 text-white hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                onclick="window.location.href = '{{ route('news.create') }}'">
                Add
            </button>

        </header>

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            #
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Title
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Theme
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Author
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Date
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($news as $index => $new)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <th scope="row"
                                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $index + 1 }} </th>
                            <td class="px-6 py-4">
                                {{ $new->title }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $new->themes->name??'' }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $new->author->name ??'' }}
                            </td>
                            <td class="px-6 py-4">
                                {{ date('d-m-y', strtotime($new->created_at)) }}
                            </td>
                            <td class="px-6 py-4">
                                <a href="{{ route('news.show', ['id' => $new->id]) }}"
                                    class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit</a>
                                <button title="Delete" data-modal-toggle="deleteNews"
                                    data-route="{{ route('news.destroy', ['id' => $new->id]) }}"
                                    data-modal-target="deleteNews"
                                    class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Delete</button>
                            </td>

                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        @include('pages.news.delete')
    </div>
    
</x-app-layout>


<script>
    $(document).ready(function() {

        $(document).on('click', '[data-modal-target="deleteNews"]', function() {
            var action = $(this).data('route');
            // console.log(action);
            $('#deleteNew').attr('action', action);
        });


    });
</script>