<x-guest-layout>
    <div class="container mx-auto ">
        <div class="max-w-[85rem] px-4 py-10 sm:px-6 lg:px-8 lg:py-14 mx-auto">
            <!--
Install the "flowbite-typography" NPM package to apply styles and format the article content:

URL: https://flowbite.com/docs/components/typography/
-->

            <main class="pt-8 pb-16 lg:pt-16 lg:pb-24 bg-white dark:bg-gray-900 antialiased">
                <div class="flex justify-between px-4 mx-auto max-w-screen-xl ">
                    <article
                        class="mx-auto w-full max-w-2xl format format-sm sm:format-base lg:format-lg format-blue dark:format-invert">
                        <header class="mb-4 lg:mb-6 not-format">
                            <address class="flex items-center mb-6 not-italic">
                                <div class="inline-flex items-center mr-3 text-sm text-gray-900 dark:text-white">
                                    <img class="mr-4 w-16 h-16 rounded-full"
                                        src="https://flowbite.com/docs/images/people/profile-picture-2.jpg"
                                        alt="Jese Leos">
                                    <div>
                                        <a href="#" rel="author"
                                            class="text-xl font-bold text-gray-900 dark:text-white">{{$news->author->name}}</a>
                                    
                                        <p class="text-base text-gray-500 dark:text-gray-400"><time pubdate
                                                datetime="2022-02-08" title="February 8th, 2022">
                                                {{ $news->created_at->format('M. j, Y') }}                                            </time></p>
                                    </div>
                                </div>
                            </address>
                            <h1
                                class="mb-4 text-3xl font-extrabold leading-tight text-gray-900 lg:mb-6 lg:text-4xl dark:text-white">
                                {{ $news->title }}</h1>
                        </header>
                        <div>
                            @isset($news->thumbnail_path)
                            <figure class="mb-4">
                            <img  src="{{ asset($news->thumbnail_path) ?? asset('images/noImage.jpg') }}" alt="Thumbnmai;">
                            @endisset

                            </figure>
                            {!! $news->content !!}
                        </div>

                    </article>
                </div>
            </main>



        </div>
    </div>
</x-guest-layout>
