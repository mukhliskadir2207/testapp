<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400..700&display=swap" rel="stylesheet" />
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.1/flowbite.min.css" rel="stylesheet" />
    {{-- @vite(['resources/css/app.css', 'resources/js/app.js']) --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.1/flowbite.min.js"></script>

</head>

<body class="bg-gray-100">


    <div class="container mx-auto ">


        <nav class="bg-white border-gray-200 dark:bg-gray-900">
            <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
                <a href="https://flowbite.com/" class="flex items-center space-x-3 rtl:space-x-reverse">
                    <img src="https://flowbite.com/docs/images/logo.svg" class="h-8" alt="Flowbite Logo" />
                    <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Flowbite</span>
                </a>
                <button data-collapse-toggle="navbar-default" type="button"
                    class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                    aria-controls="navbar-default" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 17 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M1 1h15M1 7h15M1 13h15" />
                    </svg>
                </button>
                <div class="hidden w-full md:block md:w-auto" id="navbar-default">
                    <ul
                        class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 rtl:space-x-reverse md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                        <li>
                            <a href="#"
                                class="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500"
                                aria-current="page">Home</a>
                        </li>
                        <li>
                            <a href="{{route('login')}}"
                                class="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Login</a>
                        </li>
                    
                    </ul>
                </div>
            </div>
        </nav>


        <!-- Card Blog -->
        <div class="max-w-[85rem] px-4 py-10 sm:px-6 lg:px-8 lg:py-14 mx-auto">
            <!-- Title -->
            <div class="max-w-2xl text-center mx-auto mb-10 lg:mb-14">
                <h2 class="text-2xl font-bold md:text-4xl md:leading-tight dark:text-white">Read our latest news</h2>
                <p class="mt-1 text-gray-600 dark:text-gray-400">We've helped some great companies brand, design and
                    get to market.</p>
            </div>
            <!-- End Title -->

            <!-- Grid -->
            <div class="grid sm:grid-cols-2 lg:grid-cols-4 gap-6 mb-10 lg:mb-14" id="news-container">
                <!-- Card -->
                @foreach ($news as $item)
                    <a class="group flex flex-col bg-white border shadow-sm rounded-xl hover:shadow-md transition dark:bg-slate-900 dark:border-gray-800 dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                        href="{{ route('newsfeed', ['id' => $item->id]) }}">
                        <div class="aspect-w-16 aspect-h-9">
                            <img class="w-full object-cover rounded-t-xl"
                                src="{{ asset($item->thumbnail_path) ?? asset('images/noImage.jpg') }}" alt="Thumbnail">

                        </div>
                        <div class="p-4 md:p-5">
                            <p class="mt-2 text-xs uppercase text-gray-600 dark:text-gray-400">
                                {{ $item->themes->name ?? 'No Theme' }}
                            </p>
                            <h3
                                class="mt-2 text-lg font-medium text-gray-800 group-hover:text-blue-600 dark:text-gray-300 dark:group-hover:text-white">
                                {{ $item->title }}
                            </h3>
                        </div>
                    </a>
                @endforeach


            </div>

            <!-- End Card -->

        </div>
        <!-- End Card Blog -->

    </div>

</body>

</html>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>

<script>
    Pusher.logToConsole = true;

    var pusher = new Pusher('36342319f29d2f7b67cf', {
        cluster: 'ap1'
    });

    var channel = pusher.subscribe('news-channel');
    channel.bind('my-event', function(data) {
        var newsContainer = document.getElementById('news-container');
        window.assetBaseUrl = '{{ asset('') }}';
        window.newsfeedRoute = @json(route('newsfeed', ['id' => ':id']));

        var newCardHTML = `
        <a href="${window.newsfeedRoute.replace(':id', data.news.id)}" class="group flex flex-col bg-white border shadow-sm rounded-xl hover:shadow-md transition dark:bg-slate-900 dark:border-gray-800 dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">
            <div class="aspect-w-16 aspect-h-9">
                <img class="w-full object-cover rounded-t-xl" src="${window.assetBaseUrl}${data.news.thumbnail_path ? data.news.thumbnail_path : 'images/noImage.jpg'}" alt="Thumbnail">
            </div>
            <div class="p-4 md:p-5">
                <p class="mt-2 text-xs uppercase text-gray-600 dark:text-gray-400">${data.news.theme ? data.news.theme : 'No Theme'}</p>
                <h3 class="mt-2 text-lg font-medium text-gray-800 group-hover:text-blue-600 dark:text-gray-300 dark:group-hover:text-white">${data.news.title}</h3>
            </div>  
        </a>
    `;

        newsContainer.insertAdjacentHTML('afterbegin', newCardHTML);
    });
</script>
