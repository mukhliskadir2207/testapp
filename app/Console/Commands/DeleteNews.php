<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DeleteNews extends Command
{
    protected $signature = 'news:delete';
    protected $description = 'Delete news after 2 minutes.';

    public function handle()
    {
        $twoMinutesAgo = Carbon::now()->subMinutes(2);
        $expiredNews = News::where('created_at', '<=', $twoMinutesAgo)->get();

        if ($expiredNews->isNotEmpty()) {
            News::where('created_at', '<=', $twoMinutesAgo)->delete();
            $this->info('Expired news items deleted successfully.');
        } else {
            $this->info('No expired news items found for deletion.');
        }
    }
}
