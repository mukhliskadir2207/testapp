<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class newsfeed extends Controller
{
    public function index()
    {

        $news = News::with(['author', 'themes'])
            ->latest('created_at')
            ->limit(10)
            ->get();
        return view('pages.public.index', ['news' => $news]);
    }
    public function show($id)
    {
        $news = News::with(['author', 'themes'])->findOrFail($id);

        return view('pages.public.page', compact('news'));
    }
}
