<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Theme;

use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use Illuminate\Http\Request;
use App\Events\NewsEvent;
use Storage, File;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::with(['author', 'themes'])->latest('created_at')->get();
        return view('pages.news.index', ['news' => $news]);
    }

    public function create()
    {
        $themes = Theme::all();
        return view('pages.news.create', compact('themes'));

    }

    public function store(Request $request)
    {
        // dd($request->all());
        $newsData = News::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'author_id' => $request->input('author_id'),
            'theme_id' => $request->input('theme_id'),
        ]);

        $newsId = $newsData->id;
        $imagePath = null;
        $fileName = null;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $newsId . '_' . $request->input('title') . '.' . $file->getClientOriginalExtension();
            $imagePath = 'images/thumbnail/' . $fileName;
            $newsData->update(['thumbnail_path' => $imagePath]);

            Storage::disk('public')->putFileAs('images/thumbnail', $file, $fileName);

        }


        $newsData->load('themes');
        $news = [
            'id' => $newsData->id,
            'title' => $newsData->title,
            'thumbnail_path' => $imagePath,
            'theme' => $newsData->themes->name,
        ];

        event(new NewsEvent($news));
        return redirect()->route('news.index')->with('success', 'News item created successfully.');
    }

    public function show($id)
    {
        $news = News::with(['author', 'themes'])->findOrFail($id);
        $themes = Theme::all();

        return view('pages.news.edit', compact('news', 'themes'));
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $news = News::findOrFail($id);

        $news->update([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'author_id' => $request->input('author_id'),
            'theme_id' => $request->input('theme_id'),
        ]);

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete('images/thumbnail/' . $news->thumbnail_path);


            $file = $request->file('image');
            $fileName = $news->id . '_' . $request->input('title') . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('images/thumbnail', $file, $fileName);
            $imagePath = 'images/thumbnail/' . $fileName;


        }

        $news->update(['thumbnail_path' => $imagePath]);
        $news->load('themes');


        return redirect()->route('news.index')->with('success', 'News item updated successfully.');
    }

    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();

        return redirect()->route('news.index')->with('success', 'News item deleted successfully.');
    }
}
