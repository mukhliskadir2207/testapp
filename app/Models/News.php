<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Theme;

class News extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['title', 'content', 'author_id','theme_id','thumbnail_path'];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }
    public function themes()
    {
        return $this->belongsTo(Theme::class, 'theme_id');
    }
}
