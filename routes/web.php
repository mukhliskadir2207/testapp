<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataFeedController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers;
;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::redirect('/', 'newsfeed');
Route::get('/newsfeed', [Controllers\newsfeed::class, 'index']);
Route::get('/newsfeed/{id}', [Controllers\newsfeed::class, 'show'])->name('newsfeed');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    // Route for the getting the data feed
    Route::resource('/news', Controllers\NewsController::class)->names('news')->parameters([
        'news' => 'id'
    ]);
    Route::resource('/theme', Controllers\ThemeController::class)->names('theme')->parameters([
        'theme' => 'id'
    ]);
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard/analytics', [DashboardController::class, 'analytics'])->name('analytics');
    Route::get('/dashboard/fintech', [DashboardController::class, 'fintech'])->name('fintech');


    Route::fallback(function () {
        return view('pages/utility/404');
    });
});
