FROM php:8.1.19-apache

# Install composer
ENV COMPOSER_HOME /composer
ENV PATH ./vendor/bin:/composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libzip-dev \
    libpng-dev \
    unzip
    nodejs 
    npm
RUN docker-php-ext-install zip
RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get update && apt-get install git -y
RUN apt-get update && docker-php-ext-install gd
RUN npm install

COPY . /var/www

# authorize .htaccess files
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

RUN sed -i 's/html/public/' /etc/apache2/sites-available/000-default.conf

# Apache conf
# allow .htaccess with RewriteEngine
RUN a2enmod rewrite

RUN /etc/init.d/apache2 restart
CMD ["/usr/sbin/apache2ctl","-DFOREGROUND"]

WORKDIR /var/www

RUN chmod 777 -R /var/www/bootstrap
RUN chmod 777 -R /var/www/storage
RUN composer update
RUN cp .env.example .env
# RUN php artisan key:generate
RUN mkdir /var/www/public/zip_file
RUN chmod 777 -R /var/www/public/zip_file
# RUN composer install
# RUN php artisan storage:link
# RUN chmod 777 -R /var/www/storage

# RUN php artisan jwt:secret --force